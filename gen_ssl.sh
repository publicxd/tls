#!/bin/bash

export OPENSSL_CONF=openssl.cnf

password=$1


echo 'Creating CA (ca-key.pem, ca.pem)'
echo 01 > ca.srl
openssl genrsa -des3 -passout pass:${password} -out ca-key.pem 4096
openssl req -new -passin pass:${password} \
        -subj '/CN=Non-Prod Test CA/C=SV' \
        -x509 -days 365 -key ca-key.pem -out ca.pem


echo 'Creating Swarm certificates (swarm-key.pem, swarm-cert.pem)'
openssl genrsa -des3 -passout pass:${password} -out swarm-key.pem 4096
openssl req -passin pass:${password} -subj '/CN=host01' -new -key swarm-key.pem -out swarm-client.csr
echo 'extendedKeyUsage = clientAuth,serverAuth' > extfile.cnf
openssl x509 -passin pass:${password} -req -days 365 -in swarm-client.csr -CA ca.pem -CAkey ca-key.pem -out swarm-cert.pem -extfile extfile.cnf
openssl rsa -passin pass:${password} -in swarm-key.pem -out swarm-key.pem

# Set the default keys to be Swarm
cp -rp swarm-key.pem key.pem
cp -rp swarm-cert.pem cert.pem

echo 'Creating host certificates (dockerhost01-4-key.pem, dockerhost01-3-cert.pem)'
openssl genrsa -passout pass:${password} -des3 -out dockerhost01-key.pem 4096
openssl req -passin pass:${password} -subj '/CN=host02' -new -key dockerhost01-key.pem -out dockerhost01.csr
openssl x509 -passin pass:${password} -req -days 365 -in dockerhost01.csr -CA ca.pem -CAkey ca-key.pem -out dockerhost01-cert.pem -extfile openssl.cnf
openssl rsa -passin pass:${password} -in dockerhost01-key.pem -out dockerhost01-key.pem

openssl genrsa -passout pass:${password} -des3 -out dockerhost02-key.pem 4096
openssl req -passin pass:${password} -subj '/CN=host03' -new -key dockerhost02-key.pem -out dockerhost02.csr
openssl x509 -passin pass:${password} -req -days 365 -in dockerhost02.csr -CA ca.pem -CAkey ca-key.pem -out dockerhost02-cert.pem -extfile openssl.cnf
openssl rsa -passin pass:${password} -in dockerhost02-key.pem -out dockerhost02-key.pem

openssl genrsa -passout pass:${password} -des3 -out dockerhost03-key.pem 4096
openssl req -passin pass:${password} -subj '/CN=host04' -new -key dockerhost03-key.pem -out dockerhost03.csr
openssl x509 -passin pass:${password} -req -days 365 -in dockerhost03.csr -CA ca.pem -CAkey ca-key.pem -out dockerhost03-cert.pem -extfile openssl.cnf
openssl rsa -passin pass:${password} -in dockerhost03-key.pem -out dockerhost03-key.pem

openssl genrsa -passout pass:${password} -des3 -out dockerhost04-key.pem 4096
openssl req -passin pass:${password} -subj '/CN=host05' -new -key dockerhost04-key.pem -out dockerhost04.csr
openssl x509 -passin pass:${password} -req -days 365 -in dockerhost04.csr -CA ca.pem -CAkey ca-key.pem -out dockerhost04-cert.pem -extfile openssl.cnf
openssl rsa -passin pass:${password} -in dockerhost04-key.pem -out dockerhost04-key.pem

openssl genrsa -passout pass:${password} -des3 -out build-key.pem 4096
openssl req -passin pass:${password} -subj '/CN=host06' -new -key build-key.pem -out build.csr
openssl x509 -passin pass:${password} -req -days 365 -in build.csr -CA ca.pem -CAkey ca-key.pem -out build-cert.pem -extfile openssl.cnf
openssl rsa -passin pass:${password} -in build-key.pem -out build-key.pem

# We don't need the CSRs once the cert has been generated
rm -f *.csr